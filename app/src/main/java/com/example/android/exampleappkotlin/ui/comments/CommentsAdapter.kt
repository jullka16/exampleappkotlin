package com.example.android.exampleappkotlin.ui.comments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.exampleappkotlin.databinding.ListItemCommentBinding


class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder>() {
    var data = listOf<String>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        return CommentsViewHolder.from(parent)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    class CommentsViewHolder private constructor(val binding: ListItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: String){
            binding.commentText = item
        }

        companion object{
            fun from(parent: ViewGroup): CommentsViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemCommentBinding.inflate(layoutInflater, parent, false)
                return CommentsViewHolder(binding)
            }
        }
    }

}