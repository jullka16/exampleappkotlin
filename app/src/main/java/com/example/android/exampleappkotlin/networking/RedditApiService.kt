package com.example.android.exampleappkotlin.networking

import com.example.android.exampleappkotlin.model.Listing
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL = "https://www.reddit.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
    .build()

interface RedditApiService {
    @GET("top.json")
    fun getListing(): Deferred<Listing>

    @GET("comments/{id}.json")
    fun getArticleComments(@Path("id") id: String): Deferred<List<Listing>>
}

object RedditApi{
    val retrofitService: RedditApiService by lazy { retrofit.create(RedditApiService::class.java)}
}