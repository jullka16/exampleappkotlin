package com.example.android.exampleappkotlin.model

import com.squareup.moshi.Json

data class Content(
    @Json(name = "selftext") val selfText: String = "",
    @Json(name = "title") val title: String = "",
    @Json(name = "id") val id: String = "",
    @Json(name = "body") val body: String = "",
    @Json(name = "permalink") val permalink: String = "")
