package com.example.android.exampleappkotlin.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.exampleappkotlin.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
