package com.example.android.exampleappkotlin.ui.articles

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.exampleappkotlin.model.Content
import com.example.android.exampleappkotlin.model.Listing
import java.util.*

@BindingAdapter("articlesStatus")
fun bindStatus(progressBar: ProgressBar, status: ArticlesApiStatus?) {
    when (status) {
        ArticlesApiStatus.LOADING -> progressBar.visibility = View.VISIBLE
        ArticlesApiStatus.ERROR -> progressBar.visibility = View.GONE
        ArticlesApiStatus.DONE -> progressBar.visibility = View.GONE
    }
}

@BindingAdapter("textViewStatus")
fun bindImage(textView: TextView, status: ArticlesApiStatus?) {
    when (status) {
        ArticlesApiStatus.ERROR -> textView.visibility = View.VISIBLE
    }
}

@BindingAdapter("articlesData")
fun bindRecyclerView(recyclerView: RecyclerView, listing: Listing?){
    val adapter = recyclerView.adapter as ArticlesAdapter
    val articles = ArrayList<Content>()
    val children = listing?.listingData?.children
    if(children != null){
        for (child in children) {
            articles.add(child.content)
        }
        adapter.data = articles
    }
}