package com.example.android.exampleappkotlin.ui.comments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.exampleappkotlin.model.Listing
import com.example.android.exampleappkotlin.networking.RedditApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class CommentsApiStatus { LOADING, ERROR, DONE }

class CommentsViewModel(articleId: String) : ViewModel() {
    private val _selectedArticleComments = MutableLiveData<List<Listing>>()

    val selectedArticleComments: LiveData<List<Listing>>
        get() = _selectedArticleComments

    private val _status = MutableLiveData<CommentsApiStatus>()
    val status: LiveData<CommentsApiStatus>
        get() = _status


    val viewModelJob = Job()

    val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getCommentsFromApi(articleId)
    }

    private fun getCommentsFromApi(articleId: String) {
        coroutineScope.launch {
            var getCommentsDefered = RedditApi.retrofitService.getArticleComments(articleId)
            try {
                _status.value = CommentsApiStatus.LOADING
                val listResult = getCommentsDefered.await()
                _selectedArticleComments.value = listResult
                _status.value = CommentsApiStatus.DONE
            } catch (e: Exception) {
                _status.value = CommentsApiStatus.ERROR
                _selectedArticleComments.value = ArrayList()
            }

        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}