package com.example.android.exampleappkotlin.ui.articles

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.android.exampleappkotlin.databinding.FragmentArticlesBinding
import com.example.android.exampleappkotlin.networking.BASE_URL


class ArticlesFragment : Fragment() {

    private val viewModel: ArticlesViewModel by lazy {
        ViewModelProviders.of(this).get(ArticlesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentArticlesBinding.inflate(inflater)


        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel

        val adapter = ArticlesAdapter(ArticlesAdapter.OnButtonClickListener{
            viewModel.displayComments(it)
        }, ArticlesAdapter.OnItemClickListener{
            viewModel.navigateToBrowser(it)
        })
        binding.contentList.adapter = adapter


        viewModel.navigateToSelectedComments.observe(this, Observer {
            it?.let{
                this.findNavController().navigate(ArticlesFragmentDirections.actionArticlesFragmentToCommentsFragment(it))
                viewModel.displayCommentsCompleted()
            }
        })

        viewModel.showArticleInBrowser.observe(this, Observer{
            it?.let{
                openBrowserForArticle(it)
                viewModel.navigateToBrowserCompleted()
            }
        })

        return binding.root
    }

    private fun openBrowserForArticle(premalink: String) {
        val url = BASE_URL + premalink
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}