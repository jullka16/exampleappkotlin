package com.example.android.exampleappkotlin.ui.articles

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.exampleappkotlin.model.Listing
import com.example.android.exampleappkotlin.networking.RedditApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class ArticlesApiStatus{ LOADING, ERROR, DONE }

class ArticlesViewModel : ViewModel() {
    private val _contents = MutableLiveData<Listing>()
    val content: LiveData<Listing>
        get() = _contents

    private val _navigateToSelectedComments = MutableLiveData<String>()
    val navigateToSelectedComments: LiveData<String>
        get() = _navigateToSelectedComments

    private val _showArticleInBrowser = MutableLiveData<String>()
    val showArticleInBrowser: LiveData<String>
        get() = _showArticleInBrowser

    private val _status = MutableLiveData<ArticlesApiStatus>()
    val status: LiveData<ArticlesApiStatus>
        get() = _status

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getArticlesFromApi()
    }

    private fun getArticlesFromApi() {
        coroutineScope.launch {
            var getArticlesDeferred = RedditApi.retrofitService.getListing()
            try {
                _status.value = ArticlesApiStatus.LOADING
                val listResult = getArticlesDeferred.await()
                _contents.value = listResult
                _status.value = ArticlesApiStatus.DONE
            } catch (e: Exception) {
                _contents.value = null
                _status.value = ArticlesApiStatus.ERROR
            }
        }
    }

    fun displayComments(articleId: String) {
        _navigateToSelectedComments.value = articleId
    }

    fun displayCommentsCompleted() {
        _navigateToSelectedComments.value = null
    }

    fun navigateToBrowser(articleId: String){
        _showArticleInBrowser.value = articleId
    }

    fun navigateToBrowserCompleted(){
        _showArticleInBrowser.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}