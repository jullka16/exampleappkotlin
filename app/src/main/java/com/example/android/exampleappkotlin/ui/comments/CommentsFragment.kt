package com.example.android.exampleappkotlin.ui.comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.android.exampleappkotlin.databinding.FragmentCommentsBinding

class CommentsFragment: Fragment(){
    private val viewModel: CommentsViewModel by lazy {
        val articleId = CommentsFragmentArgs.fromBundle(arguments!!).articleId
        val commentsViewModelFactory = CommentsViewModelFactory(articleId)
        ViewModelProviders.of(this, commentsViewModelFactory).get(CommentsViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentCommentsBinding.inflate(inflater)

        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel

        val adapter = CommentsAdapter()
        binding.commentsList.adapter = adapter


        return binding.root
    }
}