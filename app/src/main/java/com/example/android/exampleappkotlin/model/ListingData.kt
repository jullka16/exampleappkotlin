package com.example.android.exampleappkotlin.model

import com.squareup.moshi.Json

data class ListingData(
    //@Json(name = "dist") val dist: String = "",
    @Json(name = "children") val children: List<Child>)