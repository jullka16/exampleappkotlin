package com.example.android.exampleappkotlin.ui.comments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CommentsViewModelFactory(private val articleId: String): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(CommentsViewModel::class.java)){
            return CommentsViewModel(articleId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}