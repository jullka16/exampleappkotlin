package com.example.android.exampleappkotlin.model

import com.squareup.moshi.Json

data class Listing (
    @Json(name = "kind") val kind: String = "",
    @Json(name = "data") val listingData: ListingData)