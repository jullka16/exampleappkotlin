package com.example.android.exampleappkotlin.ui.comments

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.exampleappkotlin.model.Listing
import java.util.*

@BindingAdapter("commentsStatus")
fun bindStatus(progressBar: ProgressBar, status: CommentsApiStatus?) {
    when (status) {
        CommentsApiStatus.LOADING -> progressBar.visibility = View.VISIBLE
        CommentsApiStatus.ERROR -> progressBar.visibility = View.GONE
        CommentsApiStatus.DONE -> progressBar.visibility = View.GONE
    }
}

@BindingAdapter("commentsData")
fun bindRecyclerView(recyclerView: RecyclerView, listings: List<Listing>?) {
    val adapter = recyclerView.adapter as CommentsAdapter
    if (listings != null && listings.size > 1) {
        val children = listings.get(1).listingData.children
        val comments = ArrayList<String>()
        for (child in children) {
            comments.add(child.content.body)
        }
        adapter.data = comments
    }
}

@BindingAdapter("textViewVisibility")
fun bindTextView(textView: TextView, listings: List<Listing>?) {
    if (listings != null && listings.size > 1) {
        val children = listings.get(1).listingData.children
        if (children.size == 0) {
            textView.visibility = View.VISIBLE
        }
    }
}