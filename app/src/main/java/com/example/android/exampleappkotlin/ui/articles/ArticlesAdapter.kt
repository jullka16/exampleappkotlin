package com.example.android.exampleappkotlin.ui.articles

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.exampleappkotlin.databinding.ListItemContentBinding
import com.example.android.exampleappkotlin.model.Content

class ArticlesAdapter(val onButtonCLickListener: OnButtonClickListener, val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<ArticlesAdapter.ContentViewHolder>() {
    var data = listOf<Content>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        return ContentViewHolder.from(parent)
    }

    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        val item = data[position]
        holder.binding.commentsButton.setOnClickListener {
            onButtonCLickListener.onButtonClick(item.id)
        }
        holder.binding.articleRow.setOnClickListener {
            onItemClickListener.onItemClick(item.permalink)
        }
        holder.bind(item)
    }


    class ContentViewHolder private constructor(val binding: ListItemContentBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Content) {
            binding.content = item
        }

        companion object {
            fun from(parent: ViewGroup): ContentViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemContentBinding.inflate(layoutInflater, parent, false)
                return ContentViewHolder(binding)
            }
        }
    }

    class OnButtonClickListener(val buttonClickListener: (articleId: String) -> Unit) {
        fun onButtonClick(articleId: String) = buttonClickListener(articleId)
    }

    class OnItemClickListener(val itemClickListener: (permalink: String) -> Unit) {
        fun onItemClick(permalink: String) = itemClickListener(permalink)
    }

}